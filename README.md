# config

## awesome windows manager
- sudo apt-get install awesome
- [awesome offical wiki](https://awesome.naquadah.org/wiki/Main_Page)
- awesome wm does not use PrtScr, so bind the key to screenshot program
```lua
awful.key({ }, "Print",  function () awful.util.spawn_with_shell( "sleep 0.5 && gnome-screenshot -a")   end)
```

## tmux 
- sudo apt-get install tmux
- [tmux shortcuts & cheatsheet](https://gist.github.com/MohamedAlaa/2961058)
- Introduce tmux, not plain shortcuts list. [from digital ocean](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-tmux-on-ubuntu-12-10--2)


## bash 
- chsh -s /bin/bash 
- cat /etc/shells
```
/bin/sh
/bin/dash
/bin/bash
/bin/rbash
/usr/bin/tmux
/usr/bin/screen
/bin/zsh
/usr/bin/zsh
```

## vim

## shell
- ubuntu default: gnome-shell 
- mac os: tmux

## powerline problme 
- powerline font problem: [Install font](https://github.com/powerline/fonts)
- powerline git slow: [Disable dirty check]()

