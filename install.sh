#!/bin/bash 

# This bash script will set config for bash, tmux, vim

cp ./bashrc ~/.bashrc
source ~/.bashrc

cp ./vimrc ~/.vimrc

cd ~
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim



