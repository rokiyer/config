#!/bin/bash

$backup_path=~/.rokiyer_config/backup/
if [ -f $backup_path/.bashrc ]
then 
    cp $backup_path/.bashrc ~/
fi

if [ -f $backup_path/.vimrc ]
then 
    cp $backup_path/.vimrc ~/
fi

if [ -f $backup_path/.tmux.conf ]
then 
    cp $backup_path/.tmux.conf ~/
fi

if [ -f $backup_path/.vim ]
then 
    cp -r $backup_path/.vim ~/
fi



