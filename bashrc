# [[ alias ]]
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# [[ prompt ]]
function parse_git_branch() {
	git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}
export PS1='\[\033[0;33m\][$(date +%H:%M:%S)] \u@\h\[\033[0m\]:\[\033[0;32m\]\w\[\033[0m\]\[\033[0;31m\]$(parse_git_branch)\[\033[0m\] \n>>> '

vim_version=`vim --version | head -n 1 `

if [[ $vim_version == *"7.4"* ]]
then
	echo "vim 7.4!"
else
	alias gvim="~/vim74/bin/gvim"
	alias vim="~/vim74/bin/vim"
fi


